<%@ page import="com.j21.model.City" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 12:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@include file="/header.jsp" %>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>
<a href="/city/add">City add</a>
<table>
    <tr>
        <th width="170px">Name</th>
        <th width="170px">Continent</th>
        <th width="170px">Cities</th>
        <th width="170px"></th>
    </tr>
    <%
        List<City> cities = (List<City>) request.getAttribute("cities");
        for (City c : cities) {
            out.print("<tr>");
            out.print("<td width=\"170px\">");
            out.print(c.getName());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(c.getCountry());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(printRemoveLink(c));
            out.print("</td>");
            out.print("</tr>");
        }
    %>
</table>

</body>
</html>
<%!
    private String printRemoveLink(City c) {
        return "<a href=\"/city/remove?cityId=" + c.getId() + "\">Remove</a>";
    }
%>