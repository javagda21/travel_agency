<%@ page import="com.j21.model.Country" %>
<%@ page import="java.util.List" %>
<%@ page import="com.j21.model.City" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 1:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>City form</title>
</head>
<body>
<%@include file="/header.jsp" %>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>
<form action="/city/add" method="post">
    Name:<input type="text" name="name"><br>
    Country:
    <select name="country">
        <option value=""></option>
        <%
            List<Country> countryList = (List<Country>) request.getAttribute("countries");
            for (Country c : countryList) {
                out.print("<option value=\"" + c.getId() + "\">" + c.getName() + "</option>");
            }
        %>
    </select>
    <br>
    <input type="submit">
</form>

</body>
</html>
