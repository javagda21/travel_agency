<%@ page import="com.j21.model.Country" %>
<%@ page import="java.util.List" %>
<%@ page import="com.j21.model.City" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 1:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hotel form</title>
    <script>
        function reloadCountry(selectedCountry) {
            var selectedText = selectedCountry.options[selectedCountry.selectedIndex].innerHTML;
            window.location.href = "/hotel/add?country=" + selectedText;
        }
    </script>
</head>
<body>
<%@include file="/header.jsp" %>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>
<form action="/hotel/add" method="post">
    Name:<input type="text" name="name"><br>
    Stars:<input type="number" name="stars" min="1" max="5" step="1"><br>
    Description:<input type="text" name="description"><br>
    Country:
    <select name="country" onchange="reloadCountry(this)">
        <option value=""></option>
        <%
            List<Country> countryList = (List<Country>) request.getAttribute("countries");
            for (Country c : countryList) {
                out.print("<option value=\"" + c.getName() + "\">" + c.getName() + "</option>");
            }
        %>
    </select>
    City:
    <select name="cityId">
        <%
            List<City> cities = (List<City>) request.getAttribute("cities");
            for (City c : cities) {
                out.print("<option value=\"" + c.getId() + "\">" + c.getName() + "</option>");
            }
        %>
    </select>
    <br>
    <input type="submit">
</form>

</body>
</html>
