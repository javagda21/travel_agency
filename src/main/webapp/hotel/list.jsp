<%@ page import="com.j21.model.Hotel" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 12:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hotel list</title>
</head>
<body>
<%@include file="/header.jsp"%>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>
<a href="/hotel/add">Hotel add</a>
<table>
    <tr>
        <th width="170px">Name</th>
        <th width="170px">Stars</th>
        <th width="170px">Description</th>
        <th width="170px"></th>
    </tr>
    <%
        List<Hotel> hotels = (List<Hotel>) request.getAttribute("hotels");
        for (Hotel h : hotels) {
            out.print("<tr>");
            out.print("<td width=\"170px\">");
            out.print(h.getName());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(h.getStars());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(h.getDescription());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(printRemoveLink(h));
            out.print("</td>");
            out.print("</tr>");
        }
    %>
</table>

</body>
</html>
<%!
    private String printRemoveLink(Hotel c) {
        return "<a href=\"/hotel/remove?hotelId=" + c.getId() + "\">Remove</a>";
    }
%>