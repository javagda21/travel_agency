<%@ page import="com.j21.model.Continent" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 12:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Country form</title>
</head>
<body>
<%@include file="/header.jsp"%>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>
<form action="/country/add" method="post">
    Name:<input type="text" name="name"><br>
    Continent:
    <select name="continent">
        <%
            for (Continent c : Continent.values()) {
                out.print("<option value=\"" + c.name() + "\" >" + c.name() + "</option>");
            }
        %>
    </select>
    <br>
    <input type="submit">
</form>
</body>
</html>
