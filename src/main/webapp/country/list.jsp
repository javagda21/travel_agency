<%@ page import="com.j21.model.Country" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 12:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@include file="/header.jsp"%>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>
<a href="/country/add">Country add</a>
<table>
    <tr>
        <th width="170px">Name</th>
        <th width="170px">Continent</th>
        <th width="170px">Cities</th>
        <th width="170px"></th>
    </tr>
    <%
        List<Country> countries = (List<Country>) request.getAttribute("countries");
        for (Country c : countries) {
            out.print("<tr>");
            out.print("<td width=\"170px\">");
            out.print(c.getName());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(c.getContinent());
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(printCitiesLink(c));
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(printRemoveLink(c));
            out.print("</td>");
            out.print("</tr>");
        }
    %>
</table>

</body>
</html>
<%!
    private String printRemoveLink(Country c) {
        return "<a href=\"/country/remove?countryId=" + c.getId() + "\">Remove</a>";
    }

    private String printCitiesLink(Country c) {
        return "<a href=\"/city/list?countryId=" + c.getId() + "\">List Cities</a>";
    }
%>