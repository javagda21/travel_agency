<%@ page import="com.j21.model.Continent" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/27/19
  Time: 12:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Continent List</title>
</head>
<body>
<%@include file="/header.jsp"%>
<%= wypiszPasekNawigacyjny("http://google.com", "google") %>

<table>
    <tr>
        <th width="170px">Name</th>
        <th width="170px">Countries</th>
    </tr>
    <%
        for (Continent c : Continent.values()) {
            out.print("<tr>");
            out.print("<td width=\"170px\">");
            out.print(c);
            out.print("</td>");
            out.print("<td width=\"170px\">");
            out.print(printCountriesLink(c));
            out.print("</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
<%!
    private String printCountriesLink(Continent c) {
        return "<a href=\"/country/list?continent=" + c + "\">List countries</a>";
    }
%>