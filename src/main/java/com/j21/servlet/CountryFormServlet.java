package com.j21.servlet;

import com.j21.db.EntityDao;
import com.j21.model.Continent;
import com.j21.model.Country;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/country/add")
public class CountryFormServlet extends HttpServlet {
    private EntityDao dao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/country/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String continent = req.getParameter("continent");

        Country c = new Country(null, name, Continent.valueOf(continent));

        dao.saveOrUpdate(c);

        resp.sendRedirect("/country/list");
    }
}
