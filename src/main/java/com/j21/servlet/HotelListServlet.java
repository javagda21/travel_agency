package com.j21.servlet;

import com.j21.db.EntityDao;
import com.j21.model.Hotel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/hotel/list")
public class HotelListServlet extends HttpServlet {
    private EntityDao dao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("hotels", dao.findAll(Hotel.class));

        req.getRequestDispatcher("/hotel/list.jsp").forward(req, resp);
    }

}
