package com.j21.servlet;

import com.j21.db.EntityDao;
import com.j21.model.City;
import com.j21.model.Continent;
import com.j21.model.Country;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/city/add")
public class CityFormServlet extends HttpServlet {
    private EntityDao dao = new EntityDao();

    // @DateFormat("yyyy-MM-dd")

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("countries", dao.findAll(Country.class));

        req.getRequestDispatcher("/city/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        Long country = Long.valueOf(req.getParameter("country"));

        City city = new City(null, name, dao.findById(Country.class, country).get());

        dao.saveOrUpdate(city);

        resp.sendRedirect("/city/list");
    }
}
