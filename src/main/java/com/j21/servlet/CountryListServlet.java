package com.j21.servlet;

import com.j21.db.EntityDao;
import com.j21.model.Continent;
import com.j21.model.Country;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/country/list")
public class CountryListServlet extends HttpServlet {
    private EntityDao dao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("continent") != null) {
            req.setAttribute("countries", dao.findAll(Country.class).stream().filter(country -> country.getContinent() == Continent.valueOf(req.getParameter("continent"))).collect(Collectors.toList()));
        } else {
            req.setAttribute("countries", dao.findAll(Country.class));
        }
        req.getRequestDispatcher("/country/list.jsp").forward(req, resp);
    }
}
