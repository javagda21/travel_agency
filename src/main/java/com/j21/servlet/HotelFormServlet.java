package com.j21.servlet;

import com.j21.db.EntityDao;
import com.j21.model.City;
import com.j21.model.Country;
import com.j21.model.Hotel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/hotel/add")
public class HotelFormServlet extends HttpServlet {
    private EntityDao dao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("countries", dao.findAll(Country.class));
        if (req.getParameter("country") != null) {
            String countryName = req.getParameter("country");
            req.setAttribute("cities",
                    dao.findAll(City.class)
                            .stream()
                            .filter(city -> city.getCountry().getName().equalsIgnoreCase(countryName))
                            .collect(Collectors.toList()));
        } else {
            req.setAttribute("cities", dao.findAll(City.class)); // wszystkie
        }
        req.getRequestDispatcher("/hotel/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String desc = req.getParameter("description");
        int stars = Integer.parseInt(req.getParameter("stars"));
        Long cityId = Long.parseLong(req.getParameter("cityId"));

        Hotel h = new Hotel(null,
                name,
                stars,
                desc,
                dao.findById(City.class, cityId).get());

        dao.saveOrUpdate(h);

        resp.sendRedirect("/hotel/list");
    }
}
