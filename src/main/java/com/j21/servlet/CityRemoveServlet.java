package com.j21.servlet;

import com.j21.db.EntityDao;
import com.j21.model.City;
import com.j21.model.Country;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/city/remove")
public class CityRemoveServlet extends HttpServlet {
    private EntityDao dao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        dao.removeById(City.class, Long.parseLong(req.getParameter("cityId")));
        resp.sendRedirect("/city/list");
    }

}
